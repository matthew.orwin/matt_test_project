resource "azurerm_resource_group" "resource-gp" {
  name     = "matt-test-rg"
  location = "canadacentral"
}

resource "azurerm_virtual_network" "devtest-vnet" {
  name                = "devtest-vnet1"
  location            = "canadacentral"
  resource_group_name = "matt-test-rg"
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "devtest-snet001" {
  name                 = "snet001"
  resource_group_name  = "matt-test-rg"
  virtual_network_name = "devtest-vnet1"
  address_prefix       = "10.0.1.0/24"
}

resource "azurerm_public_ip" "test-pip" {
  name                = "devtest-pip"
  location            = "canadacentral"
  resource_group_name = "matt-test-rg"
  allocation_method   = "Static"
}

resource "azurerm_network_interface" "vm-nic" {
  name                = "vm-nic001"
  location            = "canadacentral"
  resource_group_name = "matt-test-rg"

  ip_configuration {
  name                          = "matt-test-ip"
  subnet_id                     = azurerm_subnet.devtest-snet001.id
  private_ip_address_allocation = "Static"
  private_ip_address            = "10.0.1.5"
  public_ip_address_id          = azurerm_public_ip.test-pip.id
  }
}

resource "azurerm_network_security_group" "test-nsg" {
  name                = "devtest-nsg-01"
  location            = "canadacentral"
  resource_group_name = "matt-test-rg"

   security_rule {
   name                       = "ssh"
   priority                   = 1010
   direction                  = "Inbound"
   access                     = "Allow"
   protocol                   = "Tcp"
   source_port_range          = "*"
   destination_port_range     = "22"
   source_address_prefix      = "*"
   destination_address_prefix = "*"
  }

  security_rule {
   name                       = "https"
   priority                   = 1011
   direction                  = "Inbound"
   access                     = "Allow"
   protocol                   = "Tcp"
   source_port_range          = "*"
   destination_port_range     = "443"
   source_address_prefix      = "*"
   destination_address_prefix = "*"
  }

  security_rule {
   name                       = "rdp"
   priority                   = 1012
   direction                  = "Inbound"
   access                     = "Allow"
   protocol                   = "Tcp"
   source_port_range          = "*"
   destination_port_range     = "3889"
   source_address_prefix      = "*"
   destination_address_prefix = "*"
  }
}

resource "azurerm_virtual_machine" "test-vm" {
  name                  = "devtest01-vm"
  location              = "canadacentral"
  resource_group_name   = "matt-test-rg"
  network_interface_ids = [azurerm_network_interface.vm-nic.id]
  vm_size               = "Standard_B1S"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
  storage_os_disk {
    name              = "myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "devtestvm"
    admin_username = "devadmin"
    admin_password = "Password1234!"
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
}

