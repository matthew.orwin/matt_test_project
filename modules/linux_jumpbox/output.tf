output "public_ip" {
  value = azurerm_public_ip.test-pip.ip_address
  description = "The public IP address of devtest-pip."
}

output "mac_address" {
  value = azurerm_network_interface.vm-nic.mac_address
  description = "The MAC address of vm-nic001."
}

output "private_ip_address" {
  value = azurerm_network_interface.vm-nic.private_ip_address
  description = "The private IP address of vm-nic001."
}
