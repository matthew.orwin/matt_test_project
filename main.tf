provider "azurerm" {
    version         = "1.44.0"
    client_id       = var.client_id
    client_secret   = var.client_secret
    tenant_id       = var.tenant_id
    subscription_id = var.subscription_id
}

// modules are for definitions only
module "jumpbox-test-vm" {
    source          = "./modules/linux_jumpbox"
}

output "public_ip" {
    value = module.jumpbox-test-vm.public_ip
    description = "Calls the public IP address of devtest-pip in the module."
}

output "mac_address" {
    value = module.jumpbox-test-vm.mac_address
    description = "Calls the MAC address of vm-nic001 in the module."
}

output "private_ip_address" {
    value = module.jumpbox-test-vm.private_ip_address
    description = "Calls the private IP address of vm-nic001 in the module."
}
